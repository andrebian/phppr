<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

    <div id="main-content" class="main-content">

        <?php
        if (is_front_page() && twentyfourteen_has_featured_posts()) {
            // Include the featured content template.
            get_template_part('featured-content');
        }
        ?>
        <div id="primary" class="content-area">
            <div id="content" class="site-content" role="main">

                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <?php
                    // Page thumbnail and title.
                    twentyfourteen_post_thumbnail();
                    the_title('<header class="entry-header"><h1 class="entry-title">', '</h1></header><!-- .entry-header -->');
                    ?>

                    <div class="entry-content">
                        <?php the_content(); ?>
                        <div class="contact-form">

                            <div class="row field_text alignleft">
                                <label>Seu nome (obrigatório):</label><br/>
                                <input name="yourname" value="" id="name" class="inputtext input_middle required"
                                       size="48"
                                       type="text"/>
                                <input type="hidden" id="admin-url"
                                       value="<?php echo get_admin_url(); ?>/admin-ajax.php">
                                <br/>
                                <span class="alert alert-form-error error-name">Informe seu nome</span>
                            </div>
                            <br clear="all"/><br/>

                            <div class="row field_text alignleft omega">
                                <label>Seu email (obrigatório):</label><br/>
                                <input name="email" value="" id="email" class="inputtext input_middle required"
                                       size="48"
                                       type="text"/>
                                <br/>
                                <span class="alert alert-form-error error-email">Informe seu e-mail corretamente</span>
                            </div>
                            <br clear="all"/><br/>


                            <div class="row field_textarea">
                                <label>Sua Mensagem (obrigatório):</label><br/>
                                <textarea id="message" name="message" class="textarea textarea_middle required"
                                          cols="40"
                                          rows="10"></textarea>
                                <br/>
                                <span class="alert alert-form-error error-message">Redija sua mensagem</span>
                            </div>

                            <br clear="all"/>

                            <div class="contato-form-captcha">
                                <div id="recaptcha" class="recaptcha"></div>
                                <div class="alert alert-form-error error-recaptcha">
                                    Por favor confirme o reCAPTCHA
                                </div>
                            </div>
                            <br clear="all"/>

                            <div class="row alert alert-success message-sent">
                                Mensagem
                                enviada com sucesso!
                            </div>
                            <div class="row alert alert-error message-not-sent">
                                Não foi possível enviar a mensagem.
                            </div>

                            <br clear="all"/>

                            <div class="row">
                                <input value="Enviar" title="Enviar" class="btn-submit" id="send" type="button"/>

                                <div class="loading">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/LoadingSpinner.gif">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- .entry-content -->
                </article>
                <!-- #post-## -->


            </div>
            <!-- #content -->
        </div>
        <!-- #primary -->
        <?php get_sidebar('content'); ?>
    </div><!-- #main-content -->

    <script>

        jQuery(document).ready(function ($) {

            $('#send').click(function () {

                $('.error-name').fadeOut();
                $('.error-email').fadeOut();
                $('.error-message').fadeOut();
                $('.error-recaptcha').fadeOut();
                $('.message-sent').hide();
                $('.message-not-sent').hide();

                var nome = $('#name').val();
                var email = $('#email').val();
                var mensagem = $('#message').val();
                var adminUrl = $('#admin-url').val();

                $('#send').slideUp();
                $('.loading').slideDown();

                if (nome.length < 1) {
                    $('.error-name').fadeIn();
                }

                if (email.length < 1) {
                    $('.error-email').fadeIn();
                } else {
                    if (validateEmail(email) === false) {
                        $('.error-email').fadeIn();
                    }
                }

                if (mensagem.length < 1) {
                    $('.error-message').fadeIn();
                }

                var recaptcha = grecaptcha.getResponse();
                $.ajax({
                    url: adminUrl,
                    type: 'post',
                    data: {
                        'action': 'solve_reCaptcha',
                        'recaptcha': recaptcha
                    },
                    success: function (data) {
                        if (data == 1 || data == true) {
                            $.ajax({
                                url: adminUrl,
                                type: 'post',
                                data: {
                                    'action': 'form_contato',
                                    'nome': nome,
                                    'email': email,
                                    'mensagem': mensagem
                                },
                                success: function (data) {
                                    setTimeout(function () {
                                        if (data == 'success') {
                                            $('.message-sent').show();
                                            $('#name').val('');
                                            $('#email').val('');
                                            $('#message').val('');
                                        } else {
                                            $('.message-not-sent').show();
                                        }
                                    }, 1000);

                                    grecaptcha.reset();
                                    $('.loading').hide();
                                    $('.btn-submit').show();

                                },
                                error: function (a) {
                                    //console.log(a);
                                }
                            });

                        } else {
                            $('.error-recaptcha').fadeIn();
                            $('.loading').hide();
                            $('.btn-submit').show();
                            grecaptcha.reset();
                        }
                    },
                    error: function (a) {
                        //console.log(a);
                    }
                });

                return false;
            });
        });

    </script>

    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async
            defer></script>
    <script>
        var onloadCallback = function () {
            grecaptcha.render('recaptcha', {
                'sitekey': '<?php echo RECAPTCHA_TOKEN; ?>',
                'theme': 'red'
            });
        };
    </script>

<?php
get_sidebar();
get_footer();
